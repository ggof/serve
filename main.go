package main

import (
	"fmt"
	"os"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("%s requires exactly one argument\n", os.Args[0])
		fmt.Printf("usage:\n\t%s <DIR>\n", os.Args[0])
		return
	}
	path := os.Args[1] 
	server := echo.New()

	server.Use(middleware.StaticWithConfig(middleware.StaticConfig{
		Root: path,
		HTML5: true,
	}))

	server.Logger.Fatal(server.Start(":8888"))
}